
xSize = 100;
ySize = 100;
zSize = 40;
wall = 2;

difference() {
    union() {
        difference() {
            roundedCube([xSize,ySize,zSize]);
            translate([wall,wall,wall]) roundedCube([xSize - (wall * 2),ySize - (wall * 2),zSize]);
        }
        translate([14,18,2]) standoff(3,1.5,3);
        translate([62,16,2]) standoff(3,1.5,3);
        translate([29,68,2]) standoff(3,1.5,3);
        translate([57,68,2]) standoff(3,1.5,3);

        translate([8,80,2]) standoff(5,1.5,3);
        translate([8,89,2]) standoff(5,1.5,3);
        translate([85,80,2]) standoff(5,1.5,3);
        translate([85,89,2]) standoff(5,1.5,3);

        translate([93,34,2]) standoff(9,1.5,3);
        translate([74,34,2]) standoff(9,1.5,3);

        translate([93,24,2]) standoff(5,1.5,3);
        translate([83.5,24,2]) standoff(5,1.5,3);
        translate([93,5,2]) standoff(5,1.5,3);
        translate([83.5,5,2]) standoff(5,1.5,3);
        
        
    }

    translate([22,-1,5])  cube([11,4,6]);
    translate([33,-1,5])  cube([18,4,15]);
    translate([50,-1,5])  cube([12,4,12]);
    translate([14,77,-1])  cube([62,15,4]);

    translate([83.5,63,-1])  cylinder(r=9,h=20);
    translate([83.5,13,-1])  cylinder(r=3.5,h=4);
}



module roundedCube( dimensions = [10,10,10], cornerRadius = 1, faces=32 ) {
	hull() cornerCylinders( dimensions = dimensions, cornerRadius = cornerRadius, faces=faces ); 
}

module cornerCylinders( dimensions = [10,10,10], cornerRadius = 1, faces=32 ) {
	translate([ cornerRadius, cornerRadius, 0]) {
		cylinder( r = cornerRadius, $fn = faces, h = dimensions[2] );
		translate([dimensions[0] - cornerRadius * 2, 0, 0]) cylinder( r = cornerRadius, $fn = faces, h = dimensions[2] );
		translate([0, dimensions[1] - cornerRadius * 2, 0]) {
			cylinder( r = cornerRadius, $fn = faces, h = dimensions[2] );
			translate([dimensions[0] - cornerRadius * 2, 0, 0]) cylinder( r = cornerRadius, $fn = faces, h = dimensions[2] );
		}
	}
};

module standoff( height = 10, innerRadius = 2, outerRadius = 6, raft = 1) {
    difference() {
        union() {
            cylinder(r = outerRadius, h = height);
            cylinder(r = outerRadius + raft, h = raft);
        }
        translate([0,0,1])
            cylinder(r = innerRadius, h = height);
    }
}
